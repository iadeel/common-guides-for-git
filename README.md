# README #

Git Guide

### Make Account ###

 Make acccount on Bitbucket
 Please follow to Make bitbucket account [here](https://bitbucket.org/)

### Install git locally  ###

Install Git locally on your Machine

For Windows and Mac user user this link as per your machine  [here](https://git-scm.com/download/win)

For ubuntu users user following command

sudo apt-get install git

### Choose any Git client ###

Choose any Git client from this link [here](https://git-scm.com/download/gui/linux)
I would prefer git desktop for Windows and Mac. [here](https://desktop.github.com/)
and git cola for ubuntu users.

